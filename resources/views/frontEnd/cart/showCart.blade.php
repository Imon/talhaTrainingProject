@extends('frontEnd.layout.master')
@section('title')
    Your Cart
@endsection
@section('content')
    <hr/>
        <h3 class="text-center text-success">{{Session::get('message')}}</h3>
    <hr/>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                    @foreach($cartProducts as $cartProduct)
                    <tr>
                        <td>{{$cartProduct->id}}</td>
                        <td>{{$cartProduct->name}}</td>
                        <td>BDT. {{$cartProduct->price}}</td>
                        <td>
                            <form action="{{ url('/update-cart')}}" method="post" class="">
                                {{ csrf_field() }}
                                <div class="input-group">
                                    <input type="hidden" value="{{$cartProduct->rowId}}" name="rowId">
                                    <input type="number" min="1" name="product_quantity" value="{{$cartProduct->qty}}" class="form-control">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-success">
                                            <span class="glyphicon glyphicon-upload">Update</span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </td>
                        <td>BDT. {{$cartProduct->subtotal}}</td>
                        <td>
                            <a href="{{ url('/remove-cart-product/'.$cartProduct->rowId) }}" class="btn btn-danger" onclick="return confirm('Are you sure to delete this!!');">
                                <span clas="glyphicon glyphicon-delete">Remove</span>
                            </a>
                        </td>
                    </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
<hr/>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php
                    $customerId = Session::get('customer_id');
                    if($customerId == null){ ?>
                        <a href=" {{url('/checkout')}}" class="btn btn-success pull-right">Checkout</a>
                    <?php } else{ ?>
                    <a href=" {{url('/shipping-info')}}" class="btn btn-success pull-right">Checkout</a>
                    <?php } ?>

                <a href="{{ url('/') }}" class="btn btn-success">Continue Shopping</a>
            </div>
        </div>
    </div>
@endsection