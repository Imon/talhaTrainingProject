<div class="top_bg">
    <div class="container">
        <div class="header_top">
            <div class="top_left">
                <h2><a href="#">50%off</a> use coupon code "big61" and get extra 33% off on orders above rs 2,229 </h2>
            </div>
            <div class="top_right">
                <ul>
                    <li><a href="#">Recently viewed</a></li>|
                    <li><a href="contact.html">Contact</a></li>|
                    <?php
                        $customer = Session::get('customer_id');
                        if($customer == null){ ?>
                    <li class="login" >
                        <div id="loginContainer"><a href="#" id="loginButton"><span>Login</span></a>
                            <div id="loginBox">
                                <form action="{{url('user-login-header')}}" method="post" id="loginForm">
                                    {{csrf_field()}}
                                    <fieldset id="body">
                                        <fieldset>
                                            <label for="email">Email Address</label>
                                            <input type="text" name="email" id="email">
                                        </fieldset>
                                        <fieldset>
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password">
                                         </fieldset>
                                        <input type="submit" id="login" value="Sign in">
                                        <label for="checkbox"><input type="checkbox" id="checkbox"> <i>Remember me</i></label>
                                    </fieldset>
                                    <span><a href="#">Forgot your password?</a></span>
                                </form>
                            </div>
                        </div>
                    </li>
                    <?php }else{ ?>
                    <li>Hello {{Session::get('customer_name')}}</li>|
                    <li><a href="{{url('user-logout')}}">Logout</a></li>
                    <?php } ?>
                </ul>
                <h5 class="text-right" style="color: red;">{{Session::get('message')}}</h5>
                <h5 class="text-center"  style="color: #ff0000;">{{$errors->has('email')?$errors->first('email') : ''}}</h5>
                <h5 class="text-center" style="color: #ff0000;">{{$errors->has('password')?$errors->first('password') : ''}}</h5>

            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
</body>
</html>