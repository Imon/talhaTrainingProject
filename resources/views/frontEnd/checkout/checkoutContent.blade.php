@extends('frontEnd.layout.master')
@section('title')
    Checkout
@endsection
@section('content')
    <hr/>
    <h3 class="text-center text-danger">{{Session::get('message')}}</h3>
    <hr/>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="well text-success text-center">
                    You have to login to complete your valuable order. If you are not registered then please register first.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="well">
                    <br/>
                        <h3 class="text-center text-success">You may login from here</h3>
                    <hr/>
                    <form class="form-horizontal" action="{{url('/user-login')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="control-label col-md-3">Email Address</label>
                            <div class="col-md-9">
                                <input type="email" name="email" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-9">
                                <input type="password" name="password" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <input type="submit" name="btn" class="btn btn-block btn-success" value="Login"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="well">
                    <br/>
                    <h3 class="text-center text-success">You have to register from here</h3>
                    <hr/>
                    <form class="form-horizontal" action="{{url('/new-customer')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="firstName" class="control-label col-md-3">First Name</label>
                            <div class="col-md-9">
                                <input type="text" id="firstName" name="firstName" class="form-control"/>
                                <span class="text-danger">{{$errors->has('firstName')? $errors->first('firstName'): ''}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastName" class="control-label col-md-3">Last Name</label>
                            <div class="col-md-9">
                                <input type="text" id="lastName" name="lastName" class="form-control"/>
                                <span class="text-danger">{{$errors->has('lastName')? $errors->first('lastName'): ''}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label col-md-3">Email Address</label>
                            <div class="col-md-9">
                                <input type="email" id="email" name="email" class="form-control"/>
                                <span class="text-danger">{{$errors->has('email')? $errors->first('email'): ''}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="phoneNumber">Phone Number</label>
                            <div class="col-md-9">
                                <input type="number" id="phoneNumber" name="phoneNumber" class="form-control"/>
                                <span class="text-danger">{{$errors->has('phoneNumber')? $errors->first('phoneNumber'): ''}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="control-label col-md-3">Address</label>
                            <div class="col-md-9">
                                <textarea style="resize: none;" id="address" rows="6" name="address" class="form-control">

                                </textarea>
                                <span class="text-danger">{{$errors->has('address')? $errors->first('address'): ''}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label col-md-3">Password</label>
                            <div class="col-md-9">
                                <input type="password" id="password" name="password" class="form-control"/>
                                <span class="text-danger">{{$errors->has('password')? $errors->first('password'): ''}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <input type="submit" name="btn" class="btn btn-block btn-success" value="Register"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection