@extends('frontEnd.layout.master')
@section('title')
    Checkout-Shipping Info
@endsection
@section('content')
    <hr/>
    <h3 class="text-center text-success">{{Session::get('message')}}</h3>
    <hr/>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="well text-success text-center">
                    Dear {{Session::get('customer_name')}} you have to give your shipping address.
                    If your billing and shippinf information is same then please just save.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    <br/>
                    <h3 class="text-center text-success">You have to shipping from here</h3>
                    <hr/>
                    <form class="form-horizontal" action="{{url('/new-shipping')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="fullName" class="control-label col-md-3">Full Name</label>
                            <div class="col-md-9">
                                <input type="text" value="{{$getCustomerById->firstName.' '.$getCustomerById->lastName}}" id="fullName" name="fullName" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label col-md-3">Email Address</label>
                            <div class="col-md-9">
                                <input type="email" id="email" value="{{$getCustomerById->email}}" name="email" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="phoneNumber">Phone Number</label>
                            <div class="col-md-9">
                                <input type="number" id="phoneNumber" value="{{$getCustomerById->phoneNumber}}" name="phoneNumber" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="control-label col-md-3">Address</label>
                            <div class="col-md-9">
                                <textarea style="resize: none;" id="address" rows="6" name="address" class="form-control">
                                    {{$getCustomerById->address}}
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <input type="submit" name="btn" class="btn btn-block btn-success" value="Save Shipping Info"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection