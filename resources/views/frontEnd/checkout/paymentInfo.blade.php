@extends('frontEnd.layout.master')
@section('title')
    Checkout
@endsection
@section('content')
    <hr/>
    <h3 class="text-center text-success">{{Session::get('message')}}</h3>
    <hr/>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="well text-success text-center">
                    Dear {{Session::get('customer_name')}} you have to give us product payment info. to complete your valuable order.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    <br/>
                    <h3 class="text-center text-success">Payment from here</h3>
                    <hr/>

                </div>
            </div>
        </div>
    </div>
@endsection