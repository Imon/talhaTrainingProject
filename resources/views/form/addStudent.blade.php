<h1>{{Session::get('message')}}</h1>
<hr/>
<form action="{{url('/newStudent')}}" method="POST">
    {{csrf_field()}}
    <table>
        <tr>
            <td>Name</td>
            <td><input type="text" name="studentName"></td>
        </tr>
        <tr>
            <td>Phone Number</td>
            <td><input type="number" name="number"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="btn" value="Submit"></td>
        </tr>
    </table>
</form>

