@extends('layouts.app')
@section('content')
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DataTables Advanced Tables
            </div>
            <hr/>
            <h1 class="text-center text-success">{{Session::get('message')}}</h1>
            <hr/>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Category ID</th>
                            <th>Category Title</th>
                            <th>Category Name</th>
                            <th>Category Description</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach($categories as $category)
                        <?php $i++; ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>{{$category->category_title}}</td>
                            <td>{{$category->category_name}}</td>
                            <td>{{$category->category_description}}</td>
                            <td>{{$category->publication_status == 1?'Published':'Unpublished'}}</td>
                            <td class="center">
                                @if($category->publication_status == 1)
                                <a href="{{url('/category/unpublished/'.$category->id)}}" title="Published" class="btn btn-primary btn-sm">
                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                </a>
                                @else
                                <a href="{{url('/category/published/'.$category->id)}}" title="Published" class="btn btn-warning btn-sm">
                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                </a>
                                @endif
                                <a href="{{url('/category/edit/'.$category->id)}}" title="Edit" class="btn btn-success btn-sm">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                                <form action="{{url('/category/delete')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $category->id }}">
                                    <button type="submit" name="btn" onclick="return confirm('Are you sure to delete this?')" class="btn btn-danger btn-sm">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
