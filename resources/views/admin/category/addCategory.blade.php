@extends('layouts.app')
@section('content')
<h1 class="text-center text-success">Add Category Form</h1>
<hr/>
<h1 class="text-center text-success">{{Session::get('message')}}</h1>
<hr/>
<div class="row">
    <div class="col-sm-12">
        <div class="well">
            {!!Form:: open(['url'=>'category/new', 'method'=>'post', 'class'=>'form-horizontal']) !!}
            <div class="form-group">
                {!! Form::label('category_title','Category Title', ['class'=>'control-label col-sm-2 col-sm-offset-2'])!!}
                <div class="col-sm-6">
                    {!! Form:: text('category_title', $value = null, ['class'=>'form-control']) !!}
                </div>


                {!! Form::label('category_description','Category Description', ['class'=>'control-label col-sm-2 col-sm-offset-2'])!!}
                <div class="col-sm-6">
                    {!! Form:: textarea('category_description', $value = null, ['class'=>'form-control']) !!}
                </div>

                {!! Form::label('category_name','Category Name', ['class' => 'control-label col-sm-2 col-sm-offset-2']) !!}
                <div class="col-sm-6">
                    {!! Form:: text('category_name', $value = null, ['placeholder'=>'Category Name'],['class'=>'form-control col-sm-6']) !!}
                </div>

            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-sm-offset-2" id="publication_status">Publication Status</label>
                <div class="col-sm-6">
                    <select class="form-control" name="publication_status">
                        <option>--Please Select--</option>
                        <option value="1">Published</option>
                        <option value="0">Unpublished</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-4">
                    <input type="submit" name="btn" class="btn btn-success btn-block" value="Save Category">
                </div>
            </div>
            {!!Form:: close()!!}
        </div>
    </div>
</div>
@endsection


