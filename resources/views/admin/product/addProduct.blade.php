@extends('layouts.app')
@section('content')
<h1 class="text-center text-success">Add Product Form</h1>
<hr/>
<h1 class="text-center text-success">{{Session::get('message')}}</h1>
<hr/>
<div class="row">
    <div class="col-sm-12">
        <div class="well">
            {!!Form:: open(['route'=>'new-product', 'method'=>'post', 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
                <label for="product_name" class="control-label col-sm-3">Product Name</label>
                <div class="col-sm-6">
                    <input type="text" name="product_name" id="product_name" class="form-control"/>
                    <span class="text-danger">{{ $errors->has('product_name')? $errors->first('product_name') : ''}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="category_name">Category Name</label>
                <div class="col-sm-6">
                    <select class="form-control" id="category_name" name="category_id">
                        <option value="">--Select Category Name--</option>
                        @foreach($publishedCategories as $publishedCategory)
                        <option value="{{$publishedCategory->id}}">{{$publishedCategory->category_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="manufacturer_name">Manufacturer Name</label>
                <div class="col-sm-6">
                    <select class="form-control" id="manufacturer_name" name="manufacturer_id">
                        <option value="">--Select Manufacturer Name--</option>
                        @foreach($publishedManufacturers as $publishedManufacturer) 
                        <option value="{{$publishedManufacturer->id}}">{{$publishedManufacturer->manufacturer_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="product_price" class="control-label col-sm-3">Product Price</label>
                <div class="col-sm-6">
                    <input type="text" name="product_price" id="product_price" class="form-control"/>
                    <span class="text-danger">{{ $errors->has('product_price')? $errors->first('product_price') : ''}}</span>
                </div>
            </div>
            <div class="form-group">
                <label for="product_quantity" class="control-label col-sm-3">Product Quantity</label>
                <div class="col-sm-6">
                    <input type="number" min="1" name="product_quantity" id="product_quantity" class="form-control"/>
                    <span class="text-danger">{{ $errors->has('product_quantity')? $errors->first('product_quantity') : ''}}</span>
                </div>
            </div>
            <div class="form-group">
                <label for="product_short_description" class="control-label col-sm-3">Product Short Description</label>
                <div class="col-sm-6">
                    <textarea style="resize: none" rows="6" name="product_short_description" id="product_short_description" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="product_long_description" class="control-label col-sm-3">Product Long Description</label>
                <div class="col-sm-6">
                    <textarea style="resize: none" name="product_long_description" rows="10" id="product_long_description" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Product Image</label>
                <div class="col-sm-6">
                    <input type="file" name="product_image" accept="image/*">
                    <span class="text-danger">{{ $errors->has('product_image')? $errors->first('product_image') : ''}}</span>
                </div>
            </div>
            <div class="form-group">
                <label for="publication_status" class="control-label col-sm-3">Publication Status</label>
                <div class="col-sm-6">
                    <select class="form-control" id="publication_status" name="publication_status">
                        <option>--Select--</option>
                        <option value="1">Published</option>
                        <option value="0">Unpublished</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <input type="submit" name="btn" class="btn btn-success btn-block" value="Save Product Info">
                </div>
            </div>
            {!!Form:: close()!!}
        </div>
    </div>
</div>
@endsection