@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="text-center text-success">{{Session::get('message')}}</h1>

        <div class="panel panel-default">
            <div class="panel-heading">
                DataTables Advanced Tables
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Product ID</th>
                            <th>Product Name</th>
                            <th>Category Name</th>
                            <th>Manufacturer Name</th>
                            <th>Product Price</th>
                            <th>Product Quantity</th>
                            <th>Short Description</th>
                            <th>Long Description</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach($products as $product)
                        <?php $i++; ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i; ?></td>
                            <td>{{ $product->product_name}}</td>
                            <td>{{ $product->category_name}}</td>
                            <td>{{ $product->manufacturer_name}}</td>
                            <td>{{ $product->product_price}}</td>
                            <td>{{ $product->product_quantity}}</td>
                            <td>{{ $product->product_short_description}}</td>
                            <td>{{ $product->product_long_description}}</td>
                            <td>{{ $product->publication_status == 1 ? 'Published' : 'Unpublished'}}</td>

                            <td>
                                @if($product->publication_status == 1)
                                <form action="{{route('unpublished-product')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $product->id }}"/>
                                    <button type="submit" name="btn" class="btn btn-primary btn-sm">
                                        <span class="glyphicon glyphicon-arrow-up"></span>
                                    </button>
                                </form>
                                @else
                                <form action="{{route('published-product')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $product->id }}"/>
                                    <button type="submit" name="btn" class="btn btn-warning btn-sm">
                                        <span class="glyphicon glyphicon-arrow-down"></span>
                                    </button>
                                </form>
                                @endif
                                <form action="{{route('edit-product')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $product->id }}"/>
                                    <button type="submit" name="btn" class="btn btn-success btn-sm">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                </form>
                                <form action="{{route('delete-product')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                    <button type="submit" name="btn" onclick="return confirm('Are you sure to delete this?')" class="btn btn-danger btn-sm">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
