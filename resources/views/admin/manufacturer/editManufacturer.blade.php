@extends('layouts.app')
@section('content')
<h1 class="text-center text-success">Add Category Form</h1>
<hr/>
<h1 class="text-center text-success">{{Session::get('message')}}</h1>
<hr/>
<div class="row">
    <div class="col-sm-12">
        <div class="well">
            {!!Form:: open(['route'=>'update-manufacturer', 'name'=>'editManufactureryForm', 'method'=>'post', 'class'=>'form-horizontal']) !!}
            <input type="hidden" name="id" value="{{$manufacturersById->id}}"/>
            <div class="form-group">
                {!! Form::label('manufacturer_name','Manufacturwr Name', ['class'=>'control-label col-sm-2 col-sm-offset-2'])!!}
                <div class="col-sm-6">
                    {!! Form:: text('manufacturer_name', $value = $manufacturersById->manufacturer_name, ['class'=>'form-control']) !!}
                </div>


                {!! Form::label('manufacturer_company_name','Company Name', ['class'=>'control-label col-sm-2 col-sm-offset-2'])!!}
                <div class="col-sm-6">
                    {!! Form:: text('manufacturer_company_name', $value = $manufacturersById->manufacturer_company_name, ['class'=>'form-control']) !!}
                </div>
                
                {!! Form::label('manufacturer_address','Manufacturer Address', ['class' => 'control-label col-sm-2 col-sm-offset-2']) !!}
                <div class="col-sm-6">
                    {!! Form:: textarea('manufacturer_address', $value = $manufacturersById->manufacturer_address,['class'=>'form-control col-sm-6']) !!}
                </div>

            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-sm-offset-2" for="publication_status">Publication Status</label>
                <div class="col-sm-6">
                    <select class="form-control" id="publication_status" name="publication_status">
<!--                        <option>{{$manufacturersById->publication_status == 1? 'Published' : 'Unpublished'}}</option>-->
                        <option value="1">Published</option>
                        <option value="0">Unpublished</option>
                    </select>
                    
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-sm-offset-2" for="mobile_number">Mobile Number</label>
                <div class="col-sm-6">
                    <input type="number" name="mobile_number" value="{{$manufacturersById->mobile_number}}" id="mobile_number" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-sm-offset-2" for="email">Email</label>
                <div class="col-sm-6">
                    <input type="email" name="email" value="{{$manufacturersById->email}}" id="email" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-4">
                    <input type="submit" name="btn" class="btn btn-success btn-block" value="Update Manufacturer Info">
                </div>
            </div>
            {!!Form:: close()!!}
        </div>
    </div>
</div>
<script>
    document.forms['editManufactureryForm'].elements['publication_status'].value='{{$manufacturersById->publication_status}}';
    document.forms['editManufactureryForm'].elements['category_name'].value='{{$manufacturersById->category_name}}';
</script>

@endsection


