@extends('layouts.app')
@section('content')
<h1 class="text-center text-success">Manufacturer Add Form</h1>
<hr/>
<h1 class="text-center text-success">{{Session::get('message')}}</h1>
<hr/>
<div class="row">
    <div class="col-sm-12">
        <div class="well">
            {!!Form:: open(['route'=>'new-manufacturer', 'method'=>'post', 'class'=>'form-horizontal']) !!}
            <div class="form-group">
                {!! Form::label('manufacturer_name','Manufacturer Name', ['class'=>'control-label col-sm-2 col-sm-offset-2'])!!}
                <div class="col-sm-6">
                    {!! Form:: text('manufacturer_name', $value = null, ['class'=>'form-control']) !!}
                    <span class="text-danger">{{$errors->has('manufacturer_name')? $errors->first('manufacturer_name'): ''}}</span>
                </div>


                {!! Form::label('manufacturer_address','Manufacturer Address', ['class'=>'control-label col-sm-2 col-sm-offset-2'])!!}
                <div class="col-sm-6">
                    {!! Form:: textarea('manufacturer_address', $value = null, ['class'=>'form-control']) !!}
                    <span class="text-danger">{{$errors->has('manufacturer_address')? $errors->first('manufacturer_address'):''}}</span>
                </div>

                {!! Form::label('manufacturer_company_name','Manufacturer Company Name', ['class' => 'control-label col-sm-2 col-sm-offset-2']) !!}
                <div class="col-sm-6">
                    {!! Form:: text('manufacturer_company_name',  $value = null, ['class'=>'form-control col-sm-6']) !!}
                    <span class="text-danger">{{$errors->has('manufacturer_company_name')? $errors->first('manufacturer_company_name') : ''}}</span>
                </div>

            </div>
           <div class="form-group">
                <label class="control-label col-sm-2 col-sm-offset-2" for="publication_status">Publication Status</label>
                <div class="col-sm-6">
                    <select class="form-control" id="publication_status" name="publication_status">
                        <option>--Select--</option>
                        <option value="1">Published</option>
                        <option value="0">Unpublished</option>
                    </select>
                    <span class="text-danger">{{$errors->has('publication_status') ? $errors->first('publication_status') : ''}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-sm-offset-2" for="mobile_number">Mobile Number</label>
                <div class="col-sm-6">
                    <input type="number" name="mobile_number" class="form-control" id="mobile_number"/>
                    <span class="text-danger">{{$errors->has('mobile_number')? $errors->first('mobile_number') : ''}}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2 col-sm-offset-2" for="email">Email</label>
                <div class="col-sm-6">
                    <input type="email" name="email" class="form-control" id="email"/>
                    <span class="text-danger">{{$errors->has('email')? $errors->first('email') : ''}}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-4">
                    <input type="submit" name="btn" class="btn btn-success btn-block" value="Save Category">
                </div>
            </div>
            {!!Form:: close()!!}
        </div>
    </div>
</div>
@endsection