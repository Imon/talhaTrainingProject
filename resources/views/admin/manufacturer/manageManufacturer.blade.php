@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
                    <h1 class="text-center text-success">{{Session::get('message')}}</h1>
        <div class="panel panel-default">
            <div class="panel-heading">
                DataTables Advanced Tables
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Manufacturer ID</th>
                            <th>Manufacturer Name</th>
                            <th>Company Name</th>
                            <th>Manufacturer Address</th>
                            <th>Publication Status</th>
                            <th>Mobile Number</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach($manufacturers as $manufacturer)
                        <?php $i++; ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i;?></td>
                            <td>{{ $manufacturer->manufacturer_name}}</td>
                            <td>{{ $manufacturer->manufacturer_company_name}}</td>
                            <td>{{ $manufacturer->manufacturer_address}}</td>
                            <td>{{ $manufacturer->publication_status == 1 ? 'Published' : 'Unpublished'}}</td>
                            <td>{{ $manufacturer->mobile_number}}</td>
                            <td>{{ $manufacturer->email}}</td>
                            <td>
                                @if($manufacturer->publication_status == 1)
                                <form action="{{route('unpublished-manufacturer')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $manufacturer->id }}"/>
                                    <button type="submit" name="btn" class="btn btn-primary btn-sm">
                                        <span class="glyphicon glyphicon-arrow-up"></span>
                                    </button>
                                 </form>
                                @else
                                <form action="{{route('published-manufacturer')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $manufacturer->id }}"/>
                                    <button type="submit" name="btn" class="btn btn-warning btn-sm">
                                        <span class="glyphicon glyphicon-arrow-down"></span>
                                    </button>
                                 </form>
                                @endif
                                <form action="{{route('edit-manufacturer')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $manufacturer->id }}"/>
                                    <button type="submit" name="btn" class="btn btn-success btn-sm">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                 </form>
                                <form action="{{route('delete-manufacturer')}}" method="post" style="display: inline;">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{ $manufacturer->id }}">
                                    <button type="submit" name="btn" onclick="return confirm('Are you sure to delete this?')" class="btn btn-danger btn-sm">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
