<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\category;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
//        View::share('name','Imon');
        View()->composer('frontEnd.layout.header', function(){
//            View::share('name','Imon');
            $allPublishedCategories = category::where('publication_status',1)->get();
            View::share('allPublishedCategories',$allPublishedCategories);
            
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
