<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class manufacturer extends Model {
    protected $fillable = ['manufacturer_name', 'manufacturer_company_name', 'manufacturer_address', 'publication_status', 'mobile_number', 'email'];
}
