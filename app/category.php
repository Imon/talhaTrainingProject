<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $fillable = ['category_title', 'category_description','category_name','publication_status'];
}
