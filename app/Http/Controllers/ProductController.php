<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\manufacturer;
use Image;
use App\Product;
use DB;

class ProductController extends Controller {
    public function index(){
        $publishedCategories    = category::where('publication_status', 1)->get();
        $publishedManufacturers = manufacturer::where('publication_status', 1)->get();
        return view('admin.product.addProduct', ['publishedCategories' => $publishedCategories, 'publishedManufacturers'=> $publishedManufacturers]);
    }
    public function productValidation($request){
        $this->validate($request, [
            'product_name'              => 'required',
            'product_price'             => 'required',
            'product_quantity'          => 'required',
            'product_short_description' => 'required',
            'product_long_description'  => 'required',
            'product_long_description'  => 'required',
            'product_image'             => 'required',
            ]);
    }
    public function productImageUpload($request){
        $product_image  = $request->file('product_image');
        $uploadPath     = 'product_image/';
        $extension      = $product_image->getClientOriginalExtension();
        $imageName      =  strtolower($request->product_name.'.'.$extension);
        $imageUrl       = $uploadPath.$imageName;
        
        Image::make($product_image)->resize(270, 350)->save($uploadPath.$imageName);
        return $imageUrl;
    }
    public function saveProductBasicInfo($request, $imageUrl){
        $product = new Product();
        $product->product_name              = $request->product_name;
        $product->category_id               = $request->category_id;
        $product->manufacturer_id           = $request->manufacturer_id;
        $product->product_price             = $request->product_price;
        $product->product_quantity          = $request->product_quantity;
        $product->product_short_description = $request->product_short_description;
        $product->product_long_description  = $request->product_long_description;
        $product->product_image             = $imageUrl;
        $product->publication_status        = $request->publication_status;
        $product->save();
    }

    public function saveProduct(Request $request){
        $this->productValidation($request);
        $imageUrl = $this->productImageUpload($request);
        $this->saveProductBasicInfo($request, $imageUrl);
        
        return redirect('/product/add')->with('message', 'Add success.');
    }
    public function manageProduct(){
        $products = DB::table('products')
                ->join('categories','products.category_id', '=', 'categories.id')
                ->join('manufacturers','products.manufacturer_id' , '=', 'manufacturers.id')
                ->select('products.*', 'categories.category_name', 'manufacturers.manufacturer_name')
                ->get();
//        

        return view('admin.product.manageProduct', ['products'=>$products]);
    }
    public function unPublishedProduct(Request $request){
        $unPublishedById = $request->id;
        DB::table('products')->where('id', $unPublishedById)->update(['publication_status' => 0]);
        return redirect('/product/manage')->with('message', 'Product Info unpublished Successfully');
    }
    public function publishedProduct(Request $request){
        $publishedById = $request->id;
        DB::table('products')->where('id', $publishedById)->update(['publication_status' => 1]);
        return redirect('/product/manage')->with('message', 'Product Info published Successfully');
    }
    public function editProduct(Request $request){
        return "On Construction";
    }

    public function deleteProduct(Request $request){
        $findById = Product::find($request->id);
        unlink($findById->product_image);
        $findById->delete();
        return redirect('/product/manage')->with('message', 'Product deleted Successfully');
    }
}
