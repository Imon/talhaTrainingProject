<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DhakaController extends Controller
{
    public function index(){
        $name = 'Tahlha Training';
        return view('dhaka', compact('name'));
    }
}
