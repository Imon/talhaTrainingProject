<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Illuminate\Support\Facades\Session;
use App\Shipping;

class CheckoutController extends Controller
{

    private function customerValidtion($request) {
        $this->validate($request, [
            'firstName' => 'required|max:30',
            'lastName' => 'required|max:100',
            'email' => 'required',
            'phoneNumber' => 'required|max:15',
            'address' => 'required|max:80',
            'password' => 'required|max:80',
        ]);
    }
    private function loginValidation($request){
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required|max:80',
        ]);
    }

    public function index(){
        return view('frontEnd.checkout.checkoutContent');
    }
    public function newCustomer(Request $request){
        $this->customerValidtion($request);
        $customer = new Customer();
        $customer->firstName    = $request->firstName;
        $customer->lastName     = $request->lastName;
        $customer->email        = $request->email;
        $customer->phoneNumber  = $request->phoneNumber;
        $customer->address      = $request->address;
        $customer->password     = bcrypt($request->password);
        $customer->save();
        Session::put('customer_id', $customer->id);
        Session::put('customer_name',$request->firstName.' '.$request->lastName);
        return redirect('/shipping-info');
    }
    public function ShippingInfo(){
        $customerById = Session::get('customer_id');
        $getCustomerById = Customer::find($customerById);
        return view('frontEnd.checkout.shippingInfo', ['getCustomerById' =>$getCustomerById]);
    }

    public function newShipping(Request $request){
        $shipping = new Shipping();
        $shipping->fullName = $request->fullName;
        $shipping->email = $request->email;
        $shipping->phoneNumber = $request->phoneNumber;
        $shipping->address = $request->address;
        $shipping->save();
        Session::put('shipping_id',$shipping->id);
        return redirect('/payment-info');
    }
    public function paymentInfo(){
        return view('frontEnd.checkout.paymentInfo');
    }
    public function userLogin(Request $request){
        $this->loginValidation($request);
        $email = $request->email;
        $customerByEmail = Customer::where('email', $email)->first();
        if(count($customerByEmail)>0 ){
            $existingPassword = $customerByEmail->password;
            if(password_verify($request->password, $existingPassword)){
                Session::put('customer_id',$customerByEmail->id);
                Session::put('customer_name',$customerByEmail->firstName.' '.$customerByEmail->lastName);
                return redirect('/shipping-info');
            }else {
                return redirect('/checkout')->with('message','Password not matched');
            }
        }else
            return redirect('/checkout')->with('message','Email address not matched');

    }
    public function userLoginHeader(Request $request){
        $this->loginValidation($request);
        $email = $request->email;
        $customerByEmail = Customer::where('email', $email)->first();
        if(count($customerByEmail)>0 ){
            $existingPassword = $customerByEmail->password;
            if(password_verify($request->password, $existingPassword)){
                Session::put('customer_id',$customerByEmail->id);
                Session::put('customer_name',$customerByEmail->firstName.' '.$customerByEmail->lastName);
                return redirect('/');
            }else {
                return redirect('/')->with('message','Password not matched');
            }
        }else
            return redirect('/')->with('message','Email address not matched');
    }

    public function newCustomerRegistration(Request $request){
        $this->customerValidtion($request);
        $customer = new Customer();
        $customer->firstName    = $request->firstName;
        $customer->lastName     = $request->lastName;
        $customer->email        = $request->email;
        $customer->phoneNumber  = $request->phoneNumber;
        $customer->address      = $request->address;
        $customer->password     = bcrypt($request->password);
        $customer->save();
        Session::put('customer_id', $customer->id);
        Session::put('customer_name',$request->firstName.' '.$request->lastName);
        return redirect('/');
    }
    public function userLogout(){
        Session()->forget('customer_id');
        Session()->forget('customer_name');
        return redirect('/');
    }

}
