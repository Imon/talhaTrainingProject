<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ManufacturerController extends Controller {

    private function manufacturerValidtion($request) {
        $this->validate($request, [
            'manufacturer_name' => 'required|max:30',
            'manufacturer_company_name' => 'required|max:100',
            'manufacturer_address' => 'required',
            'mobile_number' => 'required|max:15',
            'email' => 'required|max:80'
        ]);
    }

    private function createManufacturer($request) {
        $manufacturer = new \App\manufacturer();
        $manufacturer->manufacturer_name = $request->manufacturer_name;
        $manufacturer->manufacturer_company_name = $request->manufacturer_company_name;
        $manufacturer->manufacturer_address = $request->manufacturer_address;
        $manufacturer->publication_status = $request->publication_status;
        $manufacturer->mobile_number = $request->mobile_number;
        $manufacturer->email = $request->email;
        $manufacturer->save();
    }

    public function index() {
        return view('admin.manufacturer.addManufacturer');
    }

    public function saveManufacturer(Request $request) {
        $this->manufacturerValidtion($request);
        $this->createManufacturer($request);
        return redirect('/manufacturer/add')->with('message', 'Add success.');
    }

    public function manageManufacturer() {
        $manufacturers = \App\manufacturer::all();
        return view('admin.manufacturer.manageManufacturer', compact('manufacturers'));
    }

    public function editManufacturer(Request $request) {
        $manufacturersById = \App\manufacturer::find($request->id);
        return view('admin.manufacturer.editManufacturer', ['manufacturersById' => $manufacturersById]);
    }

    public function updateManufacturer(Request $request) {
        $manufacturerById = $request->id;
        $manufacturer = \App\manufacturer::find($manufacturerById);

        $manufacturer->manufacturer_name = $request->manufacturer_name;
        $manufacturer->manufacturer_company_name = $request->manufacturer_company_name;
        $manufacturer->manufacturer_address = $request->manufacturer_address;
        $manufacturer->publication_status = $request->publication_status;
        $manufacturer->mobile_number = $request->mobile_number;
        $manufacturer->email = $request->email;
        $manufacturer->save();
        return redirect('/manufacturer/manage')->with('message', 'Category Update Successfully!!');
    }

    public function deleteManufacturer(Request $request) {
        $manufacturer = \App\manufacturer::find($request->id);
        $manufacturer->delete();
        return redirect('/manufacturer/manage')->with('message', 'Successfully deleted.');
    }

    public function unpublishedManufacturer(Request $request) {
        $updateById = $request->id;
        DB::table('manufacturers')->where('id', $updateById)->update(['publication_status' => 0]);
        return redirect('/manufacturer/manage')->with('message', 'Category Info Unpublished Successfully');
    }

    public function publishedManufacturer(Request $request) {
        $updateById = $request->id;
        DB::table('manufacturers')->where('id', $updateById)->update(['publication_status' => 1]);
        return redirect('/manufacturer/manage')->with('message', 'Category Info published Successfully');
    }

}
