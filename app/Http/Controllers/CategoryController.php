<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use DB;
class CategoryController extends Controller {
    public function index(){
        return view('admin.category.addCategory');
    }
    public function saveCategory(Request $request){
        //return $request->all();
        category::create($request->all());
        return redirect('/category/add')->with('message','Category add successfully');
    }
    public function manageCategory(){
        //return "Hello";
        
        $categories = category::all();
        //return view('admin.category.manageCategory', ['categories' => $categories]);
       // return view('admin.category.manageCategory')->with('categories', $categories);
        return view('admin.category.manageCategory', compact('categories'));
    }
    public function unpublishedCategory($id){
//        $categoryById= category::find($id);
//        $categoryById->publication_status = 0;
//        $categoryById->saved();
        
       DB::table('categories')->where('id', $id)->update(['publication_status'=> 0]);
        return redirect('/category/manage')->with('message','Category Inf Unpublished Successfully');
    }
    public function publishedCategory($id){
//        $categoryById= category::find($id);
//        $categoryById->publication_status = 1;
//        $categoryById->saved();
        
        DB::table('categories')->where('id', $id)->update(['publication_status'=> 1]);
        return redirect('/category/manage')->with('message','Category Inf Unpublished Successfully');
    }
    public function editCategory($id){
        $categoryById = category::find($id);
        return view('admin.category.editCategory', ['categoryById' => $categoryById]);
    }
    public function updateCategory(Request $request){
        $categoryId = $request->id;
        $category = category::find($categoryId);
        $category->category_title = $request->category_title;
        $category->category_description = $request->category_description;
        $category->category_name = $request->category_name;
        $category->publication_status = $request->publication_status;
        $category->save();
        return redirect('/category/manage')->with('message','Category Update Successfully!!');
    }
//    public function deleteCategory($id){
//        $category = category::find($id);
//        $category->delete();
//        return redirect('/category/manage')->with('message','Category Delete Successfully!!');
//    }
        public function deleteCategory(Request $request){
        
        $category = category::find($request->id);
        $category->delete();
        return redirect('/category/manage')->with('message','Category Delete Successfully!!');
    }
    
}
