<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function index(){
        return view('calculator');
    }
    
    public function newCalculation(Request $request){
//        return $request->all();
        
        $x = $request->firstNumber;
        $y = $request->secondNumber;
        $z = $x + $y;
        return $z;
    }
}
