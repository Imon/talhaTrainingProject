<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use DB;
use App\Product;

class WelcomeController extends Controller {

    public function index() {
        $allPublishedProducts = Product::where('publication_status', 1)->get();
        return view('frontEnd.home.homecontent', ['allPublishedProducts' => $allPublishedProducts]);
    }

    public function viewByCategory($id) {
        $productsByCategoryId = Product::where('category_id', $id)->where('publication_status', 1)
                ->orderBy('id','desc')
                ->take(10)->get();
        return view('frontEnd.category.categoryContent', ['productsByCategoryId' => $productsByCategoryId]);
    }

    public function productDetails($id) {
        $productsById = Product::find($id);
        $categoryId = $productsById->category_id;
        $productsByCategoryId = Product::where(['category_id' => $categoryId, 'publication_status' => 1 ])->get();
        return view('frontEnd.details.product-details', ['productsById'=>$productsById , 'productsByCategoryId'=>$productsByCategoryId]);
    }

    public function addStudent() {
        return view('form.addStudent');
    }

    public function saveStudent(Request $request) {
        //return $request->all();
//        $student = new Student();
//        $student->studentName = $request->studentName;
//        $student->number = $request->number;
//        $student->save();
//        
//        Student::create($request->all());
        //Using Query Builder

        DB::table('students')->insert([
            'studentName' => $request->studentName,
            'number' => $request->number,
        ]);
        return redirect('/addStudent')->with('message', 'Student data saved successfully');
    }

    public function newStudent() {
        return view('');
    }
    public function customerLoginRegistration(){
        return view('frontEnd.customer.registrationLogin');
    }

}
