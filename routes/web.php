<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/', function () {
//    //return 'Dipto Shom';
//    
//    $users = [
//        'name' => 'Talha Training',
//        'city' => 'Dhaka',
//        'country' => 'Bangladesh',
//    ];
//    
//    return $users;
//});
//Route::get('/dipto', function (){
//    return 'Hello Dipto';
//});
//Route::get('/', function () {
//    return view('demo');
//});





Route::get('/', 'WelcomeController@index');
Route::get('category/{id}', 'WelcomeController@viewByCategory');
Route::get('/product-details/{id}/{name}', 'WelcomeController@productDetails');
Route::get('/login-registration','WelcomeController@customerLoginRegistration');



Route::post('/add-to-cart', 'CartController@index');
Route::get('/show-cart','CartController@cartView');
Route::post('/update-cart','CartController@updateCart');
Route::get('/remove-cart-product/{rowId}','CartController@removeCartProduct');
Route::get('checkout','CheckoutController@index');
Route::get('/user-logout','CheckoutController@userLogout');
Route::post('/new-shipping','CheckoutController@newShipping');
Route::get('/payment-info','CheckoutController@paymentInfo');
Route::post('/user-login', 'CheckoutController@userLogin');
Route::post('/user-login-header', 'CheckoutController@userLoginHeader');
Route::post('/new-customer-registration', 'CheckoutController@newCustomerRegistration');



Route::post('/new-customer','CheckoutController@newCustomer');
Route::get('shipping-info','CheckoutController@ShippingInfo');

Route::get('/addStudent', 'WelcomeController@addStudent');
Route::post('/newStudent', 'WelcomeController@saveStudent');
Auth::routes();
//Route::get('/login','WelcomeController@newStudent');

Route::get('/bangladesh', 'WelcomeController@bangladesh');
Route::get('/dhaka', 'DhakaController@index');



Route::get('/calculator', 'CalculatorController@index');
Route::post('/calculator', 'CalculatorController@newCalculation');


Route::get('/home', 'HomeController@index');


Route::get('/category/add', 'CategoryController@index');
Route::post('/category/new', 'CategoryController@saveCategory');
Route::get('/category/manage', 'CategoryController@manageCategory');
Route::get('/category/unpublished/{id}', 'CategoryController@unpublishedCategory');
Route::get('/category/published/{id}', 'CategoryController@publishedCategory');
Route::get('/category/edit/{id}', 'CategoryController@editCategory');
Route::post('/category/update', 'CategoryController@updateCategory');
Route::post('/category/delete', 'CategoryController@deleteCategory');




Route::group(['prefix' => 'manufacturer'], function() {
    Route::get('/add', 'ManufacturerController@index')->name('add-manufacturer');
    Route::post('/new', 'ManufacturerController@saveManufacturer')->name('new-manufacturer');
    Route::get('/manage', 'ManufacturerController@manageManufacturer')->name('manage-manufacturer');
    Route::post('/edit', 'ManufacturerController@editManufacturer')->name('edit-manufacturer');
    Route::post('/update', 'ManufacturerController@updateManufacturer')->name('update-manufacturer');
    Route::post('/delete', 'ManufacturerController@deleteManufacturer')->name('delete-manufacturer');
    Route::post('/unpublished', 'ManufacturerController@unpublishedManufacturer')->name('unpublished-manufacturer');
    Route::post('/published', 'ManufacturerController@publishedManufacturer')->name('published-manufacturer');
});


Route::group(['prefix' => 'product'], function() {
    Route::get('/add', 'ProductController@index')->name('add-product');
    Route::post('/new','ProductController@saveProduct')->name('new-product');
    Route::get('/manage', 'ProductController@manageProduct')->name('manage-product');
    Route::post('/unpublished','ProductController@UnpublishedProduct')->name('unpublished-product');
    Route::post('/published','ProductController@publishedProduct')->name('published-product');
    Route::post('/edit','ProductController@editProduct')->name('edit-product');
    Route::post('/delete','ProductController@deleteProduct')->name('delete-product');
});


