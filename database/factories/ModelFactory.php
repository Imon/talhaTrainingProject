<?php

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ? : $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\manufacturer::class, function (Faker\Generator $faker) {

    return [
        'manufacturer_name' => $faker->name,
        'manufacturer_address' => $faker->text,
        'manufacturer_company_name' => $faker->company,
        'publication_status' => $faker->boolean,
        'mobile_number' => $faker->numberBetween($min=0),
        'email' => $faker->email,
    ];
});
